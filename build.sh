#!/bin/env bash
cd pkgbuild
for d in ./*/ ; do (cd "$d" && ./build.sh); done
cd ../
cd x86_64
repo-add -n -R tux.db.tar.gz *.pkg.tar.zst
find . -type f -name "*.old" -delete
find . -type l -delete
for file in $(ls | egrep ".*tar.gz")
do
	mv $file $(echo "$file" | sed 's/\(.*\).tar.gz/\1/g')
done
cd ../
